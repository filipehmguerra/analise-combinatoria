//alert('ok');

var N = document.getElementById('N');
var resposta_fat = document.getElementById('fat');
var comb = document.getElementById('comb');

var P = document.getElementById('P');

function fatorial(num1){
    if (num1 < 1){
        return 1;
    } else {
        return num1*fatorial(num1-1);
    }
}


function calc_fat(){
    //alert(parseInt(N.value));

    var resultado = 1;

    for (var i = 1; i <= N.value; i++){
        resultado = resultado * i;
    }

    resposta_fat.innerHTML = resultado;
}

function calc_comb(){

        var n = N.value;
        var p = P.value;
        var permutacao;
        var arranjo;
        var combinacao;


    if (n > p){
        permutacao = fatorial(n);
        arranjo = fatorial(n)/fatorial(n-p);
        combinacao = fatorial(n)/(fatorial(p)*fatorial(n-p));

        comb.innerHTML = 'Permutações: ' + permutacao + '<br>' +
                         'Arranjos: ' + arranjo + '<br>' +
                         'Combinações: ' + combinacao;
    } else {
        comb.innerHTML = 'N deve ser maior que P';
    }
}

